# 3TP : Tu Tires ou Tu Pointes

3TP (Tu Tires ou Tu Pointes) est une application de gestion des pointages destiné à des entreprise de développement.

# Lancement du projet

## FRONT
```bash
cd <local_front_dir>
git clone https://gitlab.com/Reese73/front-tu-tires-ou-tu-pointes.git
npm install
cd 3tp
npm run serve
```

## BACK
```bash
cd <local_back_dir>
git clone https://gitlab.com/lolopop22/back-tu-tires-ou-tu-pointes.git
```

### Importer les projets MAVEN des API avec Eclipse : 
1. importer les dossiers :
    * back_3tp_booking_rest_ws
    * back_3tp_user_soap_ws
    * back_3tp_project_soap_ws
Pour chacun des projets précédent : 
2. "Run as Maven generated sources"
3. "Update Maven project"
3. Lancer chaque API avec leurs classes "Application"
4. Importer la base de données sur un serveur mySQL 

# Ports
http://localhost:8085 : Application front-end

http://localhost:8083 : API SOAP - gestion des projets

http://localhost:8084 : API SOAP - gestion des utilisateurs

http://localhost:8082 : API REST - gesion des pointages

# Equipe de 3TP 
* Loïc Assontia
* Rémi Vanelle
* Alexandre Lassiaz